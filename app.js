const weatherApi = {
    key:"---------------------",// enter own key creating api 
    baseUrl:"http://api.openweathermap.org/data/2.5/weather?"
}
  

const searchInputBox = document.getElementById('input-box');

//  Event Listerner funcation on keypress 
searchInputBox.addEventListener('keypress', (event) => {
   keyCode:Number;
    if(event.keyCode == 13) {
        console.log(searchInputBox.value);
        getWeatherReport(searchInputBox.value);
        document.querySelector('.weather-body').style.display = "block";
    }
});

 // Get Weather Report 
 function getWeatherReport(city) {
     fetch(`${weatherApi.baseUrl}q=${city}&appid=${weatherApi.key}&units=metric`)
     .then(weather => {
         return weather.json();
     }).then(showWeatherReport);
 } 


 // show weather Report 
function showWeatherReport(weather){
    console.log(weather);
 let city = document.getElementById('city');
 city.innerText = `${weather.name},${weather.sys.country}`;
  
 let temp = document.getElementById('temp');
 temp.innerHTML =`${Math.round(weather.main.temp)}&deg;C`; 

 let minMaxTemp = document.getElementById('min-max');
 minMaxTemp.innerHTML =`${Math.floor(weather.main.temp_min)}&deg;C (min)/${Math.floor(weather.main.temp_max)}&deg;C (max)`


 let weatherType = document.getElementById ('weather');
 weatherType.innerText = `${weather.weather[0].main}`
 

if(weatherType.textContent == 'Rain'){
    document.body.style.backgroundImage = "url('img/Rain.jpg')"
}else if(weatherType.textContent == 'Clouds'){
    document.body.style.backgroundImage = "url('img/Clouds.jpg')"
}
if(weatherType.textContent == 'haze'){
    document.body.style.backgroundImage = "url('https://cdn-ds.com/blogs-media/sites/77/2019/09/16153401/CarDrivingHeavyFog-A_B.jpg')"
}  

 let date = document.getElementById("date");
 let todayDate = new Date();
 date.innerText = dateManage(todayDate);


}

 // Date manage   
function dateManage(dateArg) {
 
    let days = ["sunday","monday","Tuesday","Wednesday","thurday","friday","saturday"];
    
    let months = ["janury","february","march","april","may","june","july","august","september","november","december"];

  let year = dateArg.getFullYear();
  let month = months[dateArg.getMonth()];
 
  let date = dateArg.getDate();
  let day = days[dateArg.getDay()];
  console.log(day);
   
   return `${date} ${month} (${day}), ${year}`;

}